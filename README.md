# Voter cards

A web utility for generating pre-filled PDF voter cards, for people in the Czech Republic to go print out and hand in.
Files are generated locally, as processing personal information on the server's end, even if it's not kept, is simply not neccessary in this case.
